'use strict'
// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain, ipcRenderer } = require('electron')
const path = require('path')

require('electron-reload')(__dirname)

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let messages = []

global.renderer2Window = null;

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 600,
    height: 400,
    webPreferences: {
      nodeIntegration: true
    },
  })
  console.log("create main window")

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()


  let rendererWindow
  // create renderer window
  ipcMain.on('renderer-window', () => {
    // if rendererWindow does not already exist
    if (!rendererWindow) {
      // create a new renderer window
      rendererWindow = new BrowserWindow({
        // file: path.join(__dirname, 'renderer.html'),
        width: 600,
        height: 400,
        // close with the main window
        parent: mainWindow
      })     
      // and load the renderer.html of the app.
      rendererWindow.loadFile('renderer.html')

      rendererWindow.once('ready-to-show', () => {
        rendererWindow.show()
      })

      // cleanup
      rendererWindow.on('closed', () => {
        rendererWindow = null
      })
    }
  })

  // add message from renderer window
  ipcMain.on('tell-message-to-main', (event, newMessage) => {
    messages = [ ...messages, newMessage ]
    mainWindow.send('recieve-message', messages)
  })

  // Renderer renderer2Window
  renderer2Window = new BrowserWindow ({ width: 500, height: 600 })
    renderer2Window.loadFile(path.join('renderer2', 'renderer2.html'))
    // renderer2Window.webContents.openDevTools ();
    renderer2Window.on ('closed', () => { renderer2Window = null; })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


