// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

'use strict'
const { ipcRenderer, remote } = require('electron')

let messages = []

ipcRenderer.on ('message', (event, message) => { 
    // get the messages ul
    const content = document.getElementById('content')

    let html = `<p>${message}</p>`
    messages = [ ...messages, message ]

    messages.forEach(message => {
        html = html + `<li>${message}</li>`
    })
  
    if (typeof html !== 'undefined' || html !== '') {
        // set value in html tag
        content.innerHTML = html
    }
})