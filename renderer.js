'use strict'

const { ipcRenderer } = require('electron')

document.getElementById('rendererToMainBtn').addEventListener('click', () => {
    console.log("rendererToMainBtn clicked!")
    
    const rendererToMainTextBox = document.getElementById('rendererToMainTextBox')

    // send message to main process
    ipcRenderer.send('tell-message-to-main', rendererToMainTextBox.value)

    // reset input
    rendererToMainTextBox.value = ''
})
